from api import db

# TODO: Integer choices for status

class Users(db.Model):
  """This class represents the users table."""

  __tablename__ = 'users'

  user_id = db.Column(db.Integer, primary_key=True, autoincrement = True)
  username = db.Column(db.String(50), nullable = False)
  password = db.Column(db.Text, nullable = False)
  last_logged_in = db.Column(db.DateTime, default=db.func.current_timestamp())
  access = db.Column(db.Integer, nullable = False)
  current_token = db.Column(db.String(250), nullable = False)
  clients = db.relationship('Clients', backref='users', lazy=True)

  def __init__(self, name):
    """initialize with name."""
    self.username = name

  def save(self):
    db.session.add(self)
    db.session.commit()

  @staticmethod
  def get_all():
    return Users.query.all()

  def delete(self):
    db.session.delete(self)
    db.session.commit()

  def __repr__(self):
    return "".format(self.username)
