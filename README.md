# flask-api



## Description

API built in Flask. Uses PostgreSQL for database, Flask SQLalchemy & Flask-migrate.

## Virtual Environment

Read more here: https://docs.python.org/3/tutorial/venv.html
- After environment is created, pull project inside directory
- Activate virtual environment first before proceeding to `Setup`
```
source bin/activate
```
- Change to app directory

## Setup

1. Install project dependencies

```
pip install -r requirements.txt
```

2. Create database, change URI in `DATABASE_URL` in `.env`


3. Run migrations

```
flask db upgrade
```

4. Run server

```
flask run
```

## Todo

- [ ] Update module
- [ ] Validations
- [ ] Authentication - login/logout
- [ ] Permissions for users
- [ ] Squash migrations